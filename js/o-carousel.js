$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    autoplay:true,
    autoplayTimeout: 2000, //1000 = 1s, 2000=2s
    autoplayHoverPause: false,
    responsive:{
        0:{
            items:1
        },
        360:{
            items:1
        },
        600:{
            items:3
        },
        700:{
            items:3
        },
        1000:{
            items:5
        },
    }
})

$('#resp-navbar #ham-menu-btn').click(function(){
    $('.fa-bars').toggleClass('bars-clicked');
    $('.fa-times').toggleClass('times-clicked');
    // $('#navbar').slideToggle();
    // $('affix #navbar').slideToggle();
    $('.nav-hidden').toggleClass('nav-shown');
    // $('#affix #navbar').toggleClass('navbar-toggled');
});